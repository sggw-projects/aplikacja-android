package pl.sggw.wzim.chat.model;

/**
 * Created by Michal on 05.06.2016.
 */
public interface ContactListItem {
    boolean isSectionHeader();
}
